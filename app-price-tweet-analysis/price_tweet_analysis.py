# -*- coding: utf-8 -*-
"""
Created on Sat Oct 22 07:32:59 2016

@author: prajwalshreyas
"""

import numpy as np
import pandas as pd
from pandas.tseries.offsets import BDay
import datetime
from pytz import timezone
from datetime import datetime
from datetime import timedelta
import pymysql
from apscheduler.schedulers.blocking import BlockingScheduler
import logging
import boto3
from boto3.s3.transfer import S3Transfer
from boto3.dynamodb.conditions import Key
import time
import config
from config import *
import sys

#logging.basicConfig()
# Log various event using the logging module in python
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG) #set the level to 'warning' to exclude verbose messages form botocore

# Message to indicate start of the code


log = logging.getLogger('apscheduler.executors.default')
log.setLevel(logging.INFO)  # DEBUG
fmt = logging.Formatter('%(levelname)s:%(name)s:%(message)s')

#s3 = boto3.client('s3')
s3_transfer = S3Transfer(boto3.client('s3', region_name='us-east-1',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key']))
#s3_transfer.download_file('singularity-resource-1', 'rds_config.csv', 'rds_config.csv’)
#s3.download_file('singularity-resource-1', 'rds_config.csv', '/home/ec2-user/prices/rds_config.csv')
#rds_config = pd.DataFrame()
rds_config = pd.read_csv('rds_config.csv')


rds_host  = rds_config['db_path'][0]
name = rds_config['db_username'][0]
password = rds_config['db_password'][0]
db_name = rds_config['db_name'][0]
port = 3306

def open_db():
    '''This either creates the main database or opens it'''

    try:
       conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, connect_timeout=5)
       print(conn)

    except:
       print('Error to Connect to SQL DB')

    return conn




def read_data(starttime, endtime, conn):


    dataSQL = pd.read_sql_query("select * FROM price_data.prices_2  WHERE LastTradeDateTime BETWEEN '" +  str(starttime)  +"' AND '" + str(endtime) +"';",conn)

    return dataSQL



def intraday_analysis():

    logging.warning("Start of intraday_analysis")
    # setting start and end time with a difference of one hour for reading data from SQL
    fmt = "%Y-%m-%d %H:%M:%S"
    time_delta = timedelta(hours=-1) # Delta of 1 hour
    timenow = datetime.now(timezone('US/Eastern')) # present time in NYC timezone
    time_start = timenow + time_delta # set the interval to one hour
    #start and end time in the required format
    starttime =  time_start.strftime(fmt)  # '2016-12-20 12:30:00'
    endtime =  timenow.strftime(fmt) # '2016-12-20 13:30:00'

    today_open = timenow.strftime("%Y-%m-%d") + " 09:30:00"  #  '2016-12-20 09:30:00'
    today_open_plus =  timenow.strftime("%Y-%m-%d") + " 09:40:00"  # '2016-12-20 09:40:00'

    #initial declaration of df
    intraday_ROC = pd.DataFrame()

    #Connection to SQL DB
    conn = open_db()
    cursor = conn.cursor()

    #read the days price data from SQL
    price_data = read_data(starttime, endtime, conn)
    price_data['Day_ROC'] = ""
    price_data_firstcut = read_data(today_open, today_open_plus, conn)


     #stores the first data rows of the day in a separate group
    #if timenow.strftime("%H:%M") == '10:30':
    #if endtime == '2016-11-25 10:30:00':
       #first_group = price_data.groupby('StockSymbol').first()
       #print (first_group)

    first_group = price_data_firstcut.groupby('StockSymbol').first()
    #print (first_group)

    for StockSymbol, group in price_data.groupby('StockSymbol'):

        try:
            # calculate intraday changes within a interval of 5 mins to detect spikes
            np_arr = group.as_matrix(columns=[group.columns[2]])
            float_data = [float(x) for x in np_arr]
            np_float_data = np.array(float_data)
            group['intraday_ROC'] = pd.rolling_mean(np_float_data, 1)
            #group['intraday_ROC'] = ta.ROC(np_float_data, timeperiod=1)
            group['mom_flag'] =  group['intraday_ROC']>0.5
            # Calculate cummlative change for the day
            first_price = float(first_group.loc[StockSymbol, 'Price'])

            group['Day_ROC'] = ((group['Price']/ first_price) - 1 ) * 100

            # Update the parent dataframe with the group calculations
            intraday_ROC = intraday_ROC.append(group, ignore_index=True)

        except Exception as e:
           print (str(e))
           continue

    #print (intraday_ROC)
    #sorting the companies with the positive changes at an hourly interval
    last_group = intraday_ROC.groupby('StockSymbol').last()
    last_group_pos = last_group[last_group['Day_ROC']>=0]
    last_group_pos = last_group_pos.reset_index()
    last_group_pos['p_day_decile'] = pd.qcut(last_group_pos['Day_ROC'], 10, labels=False)


    #sorting the companies with the positive changes on an hourly interval
    last_group_neg = last_group[last_group['Day_ROC']<0]
    last_group_neg = last_group_neg.reset_index()
    last_group_neg['n_day_decile'] = pd.qcut(last_group_neg['Day_ROC'].abs(), 10, labels=False)
    #print (last_group_neg)
    #Combining of positive and negative changes
    last_group_com = last_group_pos.append(last_group_neg, ignore_index=False)
    #print (last_group_com)


    #Get SP500 csv data from EC2 and clean it for merging
    sp500 = pd.DataFrame()
    sp500 = pd.read_csv('SP500.csv')
    sp500_mod = sp500[['ticker','Keyword']]
    sp500_mod = sp500_mod.dropna(axis=0)
    sp500_mod['Keyword'] = sp500_mod['Keyword'].str.lower()
    sp500_mod['Keyword'] = sp500_mod['Keyword'].str.rstrip()

    #Join the stock ticker and the keyword in a single dataframe
    last_group_join = last_group_com.merge(right = sp500_mod, how = 'left', left_on='StockSymbol', right_on='ticker')
    last_group_join.drop('ticker', axis=1, inplace=True)
    #logger.debug('price_data_join=%s', price_data_join)
    #print (last_group_join)

    #save data in S3 for acces to ES
    last_group_join.to_csv('relevance_score.csv', index=False)
    # Upload the file to S3
    s3.delete_object(Bucket='price.data', Key='relevance_score.csv')
    s3.upload_file('relevance_score.csv', 'price.data', 'relevance_score.csv')

    #Convert the dataframe to a list to insert the data one by one into SQL
    intraday_ROC_list = intraday_ROC.values.tolist()
    last_group_list = last_group_join.values.tolist()


    #Add the hourly price change data to the mySQL database table Price_mom
    for row in intraday_ROC_list:

        date = str(row[0])
        stock = str(row[1])
        s_price = str(row[2])
        s_day_roc = str(row[3])
        s_intday_roc= str(row[4])
        mom_flag= str(row[5])

        try:
           cursor.execute('''INSERT INTO Price_mom (LastTradeDateTime, StockSymbol, Price, ROC_Intraday , ROC_Day, Mom_Flag) VALUES (%s,%s,%s,%s, %s, %s);''',([date], [stock], [s_price], [s_day_roc], [s_intday_roc],[mom_flag]))

        except Exception as e:
           print (str(e))
           continue

        conn.commit()
        #logging.warning("completed committ Price_mom")
    #Add the hourly Relevance score data to the mySQL database table Price_mom

    for row in last_group_list:

        date = str(row[1])
        stock = str(row[3])
        s_price = str(row[2])
        s_day_roc = str(row[0])
        s_intday_roc= str(row[4])
        mom_flag= str(row[5])
        n_day_decile= str(row[6])
        p_day_decile= str(row[7])
        keyword = str(row[8])


        try:
          cursor.execute('''INSERT INTO Relevance_Score (LastTradeDateTime, StockSymbol, Price, ROC_Intraday , ROC_Day, Mom_Flag, Positive_Day_Decile, Negative_Day_Decile, keyword) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s);''',([date], [stock], [s_price],[s_intday_roc], [s_day_roc],[mom_flag], [p_day_decile], [n_day_decile],[keyword]))

        except Exception as e:
              print (str(e))
              continue

    conn.commit()
    conn.close()
    #logging.warning("completed execution rel_score")

    return 0


def EOD_Change():

     conn = open_db()
     cursor = conn.cursor()
     Category = "EOD"

     #Setting the day range for getting data
     today = pd.datetime.today()
     today_date = today.strftime("%Y-%m-%d") + " 16:30:00"
     today_date_open = today.strftime("%Y-%m-%d") + " 09:30:00"
     prev_bday = today - BDay(1) #calculates previous business day
     previous_date = prev_bday.strftime("%Y-%m-%d") +  " 09:00:00"



     mask_date = prev_bday.strftime("%Y-%m-%d") + " 16:30:00" #Used to filter for previous close price range

     price_data= read_data(previous_date , today_date, conn)

     EOD_all = pd.DataFrame()
     EOD_all_com = pd.DataFrame()

     for StockSymbol, group in price_data.groupby('StockSymbol'):
         try:
             mask = (group['LastTradeDateTime']>= previous_date) & (group['LastTradeDateTime']< mask_date)
             prev_day_price = group[mask]
             #print (prev_day_price)
            # get the previous day close
             prev_close = prev_day_price.iloc[-1]['Price']


             # get current day open
             current_day_price = group[group['LastTradeDateTime']>= today_date_open]
             curr_close = current_day_price.iloc[-1]['Price']

             #print (prev_close)
             #print (curr_close)


             change = ((float(curr_close) - float(prev_close))/float(prev_close))*100
             EOD_updated = pd.DataFrame({'LastTradeDateTime':[today_date], 'StockSymbol': [StockSymbol], 'prev_close':[prev_close],'curr_close': [curr_close], 'per_change':[change]})

             EOD_all = EOD_all.append(EOD_updated, ignore_index=False)

         except Exception as e:
              print (str(e))
              continue

         # split of the positive change in stock price and decile rank
     EOD_all_pos = EOD_all[EOD_all['per_change']>=0]
     EOD_all_pos['p_decile'] = pd.qcut(EOD_all_pos['per_change'], 10, labels=False)

    # split of the negative change in stock price and decile rank
     EOD_all_neg = EOD_all[EOD_all['per_change']<0]
     EOD_all_neg['n_decile'] = pd.qcut(EOD_all_neg['per_change'], 10, labels=False)

    #Combining of positive and negative changes
     EOD_all_com = EOD_all_pos.append(EOD_all_neg, ignore_index=False)


          #Convert the dataframe to a list to insert the data one by one into SQL
     EOD_all_list = EOD_all_com.values.tolist()

    #Add the data to the mySQL database
     for row in EOD_all_list:

            date = str(row[0])
            stock = str(row[1])
            curr_price = str(row[2])
            n_decile_rank = str(row[3])
            p_decile_rank = str(row[4])
            change = str(row[5])
            prev_price = str(row[6])

            try:
               cursor.execute('''INSERT INTO Price_Change (LastTradeDateTime, StockSymbol, Prev_Close, Curr_Close, Per_Change, Category, n_Decile_Rank, p_Decile_Rank ) VALUES (%s,%s,%s,%s,%s,%s, %s, %s);''',([date], [stock], [prev_price], [curr_price], [change], [Category], [n_decile_rank], [p_decile_rank]))

            except Exception as e:
                 print (str(e))
                 continue

     conn.commit()
     conn.close()
     return 0

def keyword_analysis():

    tweets_analysis = pd.DataFrame(columns=('keyword', 'Count'))
    fmt = "%Y-%m-%d %H:%M:%S"
    #logging.warning("keyword_analysis function start")

    # --------------------------------------- Connect to dynamodb
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1',endpoint_url= 'http://dynamodb.us-east-1.amazonaws.com',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key'])
    table = dynamodb.Table('tweets_SP500_Q12017')
    # Get the start and end time for processing tweets
    edate = datetime.now()
    edate_utc = int(edate.strftime('%s')) * 1000
    sdate_utc = edate_utc - (3600 * 1000)
    edate = edate.strftime(fmt)

    #Get the keyword data for the query from a csv file
    searchSP = pd.read_csv('SP500.csv')#/Users/prajwalshreyas/Desktop/Singularity/StaticData/Twitter/twitterSP500.csv
    #logging.warning("search keyword csv")
    searchSP = searchSP.replace(np.nan,'', regex=True) # replave NAN values with blank
    searchSP['Keyword'] = searchSP['Keyword'].str.lower()
    searchNameSP = searchSP['Keyword'].tolist()
    searchNameSP = filter(None, searchNameSP) # to remove blanks
    searchNameSP = [x.strip(' ') for x in searchNameSP] # to remove trailing and leading spaces
    searchNameSP = searchNameSP[:400]
    #logging.warning(searchNameSP)
    #print(searchNameSP)
    row = 0

    for item in searchNameSP:
        #logging.warning(item)
        response = table.query(IndexName='keyword-timestamp-index', Select= 'COUNT', KeyConditionExpression= Key('keyword').eq(item) & Key('timestamp').between(sdate_utc,edate_utc) )
        #print (response)

        if response['Count'] != 0:

           tweets_analysis.loc[row] = [item ,response['Count']]
           row = row + 1

           time.sleep(0.01)
     #Convert the dataframe to a list to insert the data one by one into SQL

    tweet_analysis_list = tweets_analysis.values.tolist()
    #print (tweet_analysis_list)
     #Connection to SQL DB
    conn = open_db()
    cursor = conn.cursor()

     #Add the hourly tweet metrics data to the mySQL database table tweet_analytics
    for row in tweet_analysis_list:

        date = str(edate)
        keyword = str(row[0])
        count = str(row[1])

        try:
           cursor.execute('''INSERT INTO tweet_analytics (timestamp_run, keyword, count) VALUES (%s,%s,%s);''',([date], [keyword], [count]))

        except Exception as e:
            print (str(e))
            continue

    conn.commit()
    conn.close()

    return 0


#intraday_analysis()
#EOD_Change()
#logging.warning("scheduler started")
scheduler = BlockingScheduler(timezone='America/New_York')
scheduler.add_job(intraday_analysis,'cron', day_of_week='mon-fri',hour='10-16', minute='30')
scheduler.add_job(EOD_Change,'cron', day_of_week='mon-fri',hour='17')
scheduler.add_job(keyword_analysis,'interval',hours=1)
#logging.warning("keyword_analysis scheduler scheduled")
#scheduler.add_job(EOD_Change,'interval',seconds=30)
scheduler.start()
#logging.warning("keyword_analysis scheduler started")
#logging
h = logging.StreamHandler()
h.setFormatter(fmt)
log.addHandler(h)
