FROM ujwal19/alpine-py3-numpy-pandas

# File Author / Maintainer
MAINTAINER Prajwal Shreyas

# install sdk for alpine linux this is no install 3rd party python packages
RUN apk add --update --no-cache \
    python3-dev \
    build-base

#Install jupyter kernel gateway
#RUN pip install jupyter_kernel_gateway

# Copy the application folder inside the container
ADD /app-price-tweet-analysis /app-price-tweet-analysis

# Copy the ta-lib folder inside the container
# ADD /ta-lib /ta-lib


# Get pip to download and install requirements:
RUN pip install -r /app-price-tweet-analysis/requirements.txt

# Expose ports
# EXPOSE 8003

# Set the default directory where CMD will execute
WORKDIR /app-price-tweet-analysis
#CMD ./configure --prefix=/usr
#CMD make
#CMD make install
#CMD pip install ta-lib
CMD python3 /app-price-tweet-analysis/price_tweet_analysis.py
